/* Generated by: JJTree: Do not edit this line. ASTTerm.java Version 1.1 */
/* ParserGeneratorCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package com.kitfox.svg.animation.parser;

public
class ASTTerm extends SimpleNode {
  public ASTTerm(int id) {
    super(id);
  }

  public ASTTerm(AnimTimeParser p, int id) {
    super(p, id);
  }

}
/* ParserGeneratorCC - OriginalChecksum=1ba3d64975750e7f691f5a4a4f5d10e2 (do not edit this line) */
