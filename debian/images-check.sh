#!/bin/sh

PKGDIR="$(dirname "$0")"
PARENTDIR="$(dirname "${PKGDIR}")"

OUTPUT=$(grep "font-family:'Droid Sans'" "${PARENTDIR}/resources/images/" -r 2> /dev/null)

if [ -n "${OUTPUT}" ]; then
	echo "Images using Droid font:"
	echo "${OUTPUT}"

	exit 1
fi

exit 0
