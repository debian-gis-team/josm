Source: josm
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: David Paleino <dapal@debian.org>,
           Bas Couwenberg <sebastic@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk (>= 2:1.11) | java11-sdk,
               ant,
               ant-contrib,
               gettext,
               javacc,
               jmapviewer (>= 2.24),
               libgettext-ant-tasks-java,
               libgettext-commons-java (>= 0.9.6),
               libterm-readkey-perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/josm
Vcs-Git: https://salsa.debian.org/debian-gis-team/josm.git
Homepage: https://josm.openstreetmap.de
Rules-Requires-Root: no

Package: josm
Architecture: all
Depends: default-jre (>= 2:1.11) | java11-runtime,
         fonts-noto,
         jmapviewer (>= 2.24),
         libgettext-commons-java (>= 0.9.6),
         proj-data,
         ${misc:Depends}
Recommends: josm-l10n (= ${source:Version}),
            openjfx
Conflicts: josm-plugins
Description: Editor for OpenStreetMap
 JOSM is an editor for OpenStreetMap (OSM) written in Java.
 The current version supports stand alone GPX tracks, GPX track data
 from OSM database and existing nodes, line segments and metadata tags
 from the OSM database.
 .
 OpenStreetMap is a project aimed squarely at creating and providing
 free geographic data such as street maps to anyone who wants them.
 The project was started because most maps you think of as free actually
 have legal or technical restrictions on their use, holding back people
 from using them in creative, productive or unexpected ways.

Package: josm-l10n
Architecture: all
Section: localization
Depends: ${misc:Depends}
Description: Editor for OpenStreetMap - translation files
 JOSM is an editor for OpenStreetMap (OSM) written in Java.
 The current version supports stand alone GPX tracks, GPX track data
 from OSM database and existing nodes, line segments and metadata tags
 from the OSM database.
 .
 OpenStreetMap is a project aimed squarely at creating and providing
 free geographic data such as street maps to anyone who wants them.
 The project was started because most maps you think of as free actually
 have legal or technical restrictions on their use, holding back people
 from using them in creative, productive or unexpected ways.
 .
 This package contains the translation files for JOSM.
